  # Sobre
  **Autor:** Bernardo Schmitz dos Santos   
  **Data:** 18/03/2020  
  **Versão:** 1.2  
  **Placa utilizada:** WeMos D1 R2   (ESP8266MOD)  
  **Última atualização:** 17/05/2020  

  ![Tela principal](https://i.imgur.com/uhkv7SR.png)
  ![Monitoramento](https://i.imgur.com/lzrjm6r.png)

  # Descrição:  
  
  Este projeto consiste em um sistema de controle e monitoramento para uma estufa de hortaliças, baseado em Internet das Coisas e microcontroladores.

  As variáveis controladas são: 
  - Umidade relativa do ar;
  - Temperatura;
  - Umidade do solo;
  - Luminosidade.
  
  As variáveis são lidas por meio dos sensores DHT22 (umidade e temperatura), um sensor resistivo de umidade do solo e um sensor LDR (Resistor dependente de luz) que detecta a presença ou não de luz.

  Os dados obtidos são enviados a um canal na plataforma de IoT, [ThingSpeak](https://thingspeak.com/channels/1031479)


## Relação dos pinos e dispositivos conectados.

>As portas TX e RX devem estar desconectadas durante o upload do código.

| Código | Porta |      Sensor       |
|--------|-------|-------------------|
|    0   |   D3  | DHT22 Interno     |
|    1   |   TX  | Relé Umidificador |
|    2   |   D4  | DHT22 Externo     | 
|    3   |   RX  | Cooler Traseiro   |
|    4   |   D2  |       N/A         |
|    5   |   D1  |       N/A         |
|    12  |   D6  | Relé Lâmpada      |
|    13  |   D7  | Cooler Frontal    |
|    14  |   D5  | Relé Bomba d'Água |
|    15  |   D8  |       N/A         |
|    16  |   D0  |       LDR         |
|    A0  |   A0  | Umidade do Solo   |

> Para fazer uso dos pinos TX e RX como portas digitais, é necessário não utilizar o comando Serial.begin, responsável pela comunicação serial da placa com o computador.

