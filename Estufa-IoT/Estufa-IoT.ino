// Bibliotecas
#include <Arduino.h>
#include <ESPAsyncTCP.h>
#include <ESPAsyncWebServer.h>
#include <Hash.h>
#include <FS.h>
#include <ThingSpeak.h>
#include <ESP8266WiFi.h>
#include "DHT.h"       // Sensor de temperatura e umidade relativa do ar DHT11 e DHT22
#include <NTPClient.h> // Horário
#include <WiFiUdp.h>   // Protocolo UDP

AsyncWebServer server(80); // Porta onde o servidor estará escutando. Porta 80 é a porta HTTP padrão.

// Páginas WEB
const char index_html[] PROGMEM = R"rawliteral(
<!DOCTYPE html><html lang="pt-BR"><head> <title>Estufa IoT</title> <meta charset="UTF-8"> <meta name="viewport" content="width=device-width, initial-scale=1.0"> <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous"> <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script> <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script> <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script> <script> function dados_enviados() { setTimeout(function () { document.location.reload(true); }, 3000); document.getElementById("alerta").hidden = false; } </script></head><body class="bg-light"> <nav class="navbar navbar-expand-lg navbar-light bg-transparent border-bottom"> <a class="navbar-brand" href="/"><img src="https://i.imgur.com/w0ytmLf.png" class="rounded" alt="Estufa IoT">Estufa IoT</a> <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"> <span class="navbar-toggler-icon"></span> </button> <div class="collapse navbar-collapse" id="navbarSupportedContent"> <ul class="navbar-nav mr-auto"> <li class="nav-item"> <a class="nav-link" href="/monitoramento"> <img src="https://i.imgur.com/uNMf99F.png" alt="Monitoramento"> Monitoramento</a> </li> <li class="nav-item"> <a class="nav-link" href="https://thingspeak.com/channels/1031479" target="_blank"> <img src="https://i.imgur.com/oZE1HPC.png" alt=" Canal ThingSpeak"> Canal no ThingSpeak</a> </li> <li class="nav-item"> <a class="nav-link" href="/sobre"> <img src="https://i.imgur.com/6lcZ19z.png" alt="Sobre"> Sobre</a> </li> </ul> </div> </nav> <br> <div class="container "> <div id="accordion"> <div class="card"> <div class="card-header"> <h5 class="mb-0"> <button class="btn btn-block" data-toggle="collapse" data-target="#collapseOne" aria-expanded="false" aria-controls="collapseOne"> <img src="https://i.imgur.com/r9cpkAk.png" alt="Parâmetros Atuais"> <h6>Parâmetros atuais</h6> </button> </h5> </div> <div id="collapseOne" class="collapse" aria-labelledby=" " data-parent="#accordion"> <div class="card-body"> <div class="row text-center"> <div class="col-md col-sm"> <h6>Temperatura Mínima: </h6> <span class="badge badge-dark">%tempmin%°C</span> </div> <div class="col-md col-sm"> <h6>Temperatura Máxima: </h6> <span class="badge badge-dark">%tempmax%°C</span> </div> <div class="col-md col-sm"> <h6>Umidade Mínima: </h6> <span class="badge badge-dark">%umimin%&#37</span> </div> <div class="col-md col-sm"> <h6>Umidade Máxima: </h6> <span class="badge badge-dark">%umimax%&#37</span> </div> <div class="col-md col-sm"> <h6>Hora Inicial: </h6> <span class="badge badge-dark">%horainicial%:00</span> </div> <div class="col-md col-sm"> <h6>Hora Final: </h6> <span class="badge badge-dark">%horafinal%:00</span> </div> </div> </div> </div> </div> </div> <br><br> <form action="/get" onsubmit="dados_enviados()"> <div class="form-group "> <div class="form-row"> <div class="col-md-4 col-sm"> </div> <div class="col-md-2 col-sm"> <label><img src="https://i.imgur.com/rHy2OL3.png" alt="Temperatura Mínima"> Temperatura Mín</label> <input type="number" min="-40" max="80" name="input1" class="form-control" required="true"> </div> <div class="col-md-2 col-sm"> <label><img src="https://i.imgur.com/CdSJxpn.png" alt=".Temperatura Máxima.."> Temperatura Max</label> <input type="number" min="-40" max="80" name="input2" class="form-control" required="true"> </div> <div class="col-md-4 col-sm"> </div> </div> <br> <div class="form-row"> <div class="col-md-4 col-sm"> </div> <div class="col-md-2 col-sm"> <label><img src="https://i.imgur.com/vYLAHkn.png" alt="Umidade Mínima"> Umidade Mín</label> <input type="number" min="0" max="100" name="input3" class="form-control" required="true"> </div> <div class="col-md-2 col-sm"> <label><img src="https://i.imgur.com/CbQDes9.png" alt="Umidade Mínima"> Umidade Max</label> <input type="number" min="0" max="100" name="input4" class="form-control" required="true"> </div> <div class="col-md-4 col-sm"> </div> </div> <br> <div class="form-row"> <div class="col-md-4 col-sm"> </div> <div class="col-md-2 col-sm"> <label><img src="https://i.imgur.com/MeBsdju.png" alt="Umidade Mínima"> Hora Inicial</label> <input type="number" min="0" max="23" name="input5" class="form-control" required="true"> </div> <div class="col-md-2 col-sm"> <label><img src="https://i.imgur.com/80YQ5Uk.png" alt="Umidade Mínima"> Hora Final</label> <input type="number" min="0" max="23" name="input6" class="form-control" required="true"> </div> <div class="col-md-4 col-sm"></div> </div><br> <div class="form-row"> <div class="col-md-4 col-sm"> </div> <br> <button type="submit" class="col-md-4 col-sm btn btn-success">Enviar Parâmetros</button> <div class="col-md-4 col-sm"> </div> </div> <br> <div class="alert alert-success alert-dismissible fade show" role="alert" hidden="true" id="alerta"> <strong>Dados enviados para o controlador com sucesso!</strong> A página será atualizada em alguns segundos... <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button> </div> </form> </div></body></html>
)rawliteral";
const char monitoramento_html[] PROGMEM = R"rawliteral(
<!DOCTYPE html><html lang="pt-BR"><head> <title>Estufa IoT</title> <meta charset="UTF-8"> <meta name="viewport" content="width=device-width, initial-scale=1.0"> <meta http-equiv="refresh" content="5"> <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous"> <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script> <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script> <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script></head><body class="bg-light"> <nav class="navbar navbar-expand-lg navbar-light bg-transparent border-bottom"> <a class="navbar-brand" href="/"><img src="https://i.imgur.com/w0ytmLf.png" class="rounded" alt="Estufa IoT">Estufa IoT</a> <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"> <span class="navbar-toggler-icon"></span> </button> <div class="collapse navbar-collapse" id="navbarSupportedContent"> <ul class="navbar-nav mr-auto"> <li class="nav-item"> <a class="nav-link" href="/monitoramento"> <img src="https://i.imgur.com/uNMf99F.png" alt="Monitoramento"> Monitoramento</a> </li> <li class="nav-item"> <a class="nav-link" href="https://thingspeak.com/channels/1031479" target="_blank"> <img src="https://i.imgur.com/oZE1HPC.png" alt=" Canal ThingSpeak"> Canal no ThingSpeak</a> </li> <li class="nav-item"> <a class="nav-link" href="/sobre"> <img src="https://i.imgur.com/6lcZ19z.png" alt="Sobre"> Sobre</a> </li> </ul> </div> </nav> <br> <div class="container "> <div class="row text-center"> <div class="col-md col-sm"> <h1 class='text-center text-muted'>Dados Atuais</h1><br> </div> <div class="col-md col-sm"> <h6><img src="https://i.imgur.com/XFu5D3z.png" alt="Hora atual"> Horário: %horario%</a></h6> </div> </div> <br><br> <h2 class='text-center text-muted'>Variáveis</h2><br> <div class="row text-center"> <div class="col-md col-sm"> </div> <div class="col-md col-sm"> <h6>Temperatura Interna</h6> <span class="badge badge-dark">%tempint%°C</span> </div> <div class="col-md col-sm"> <h6>Temperatura Externa</h6> <span class="badge badge-dark">%tempext%°C</span> </div> <div class="col-md col-sm"> <h6>Umidade Interna</h6> <span class="badge badge-dark">%umiint%&#37</span> </div> <div class="col-md col-sm"> <h6>Umidade Externa</h6> <span class="badge badge-dark">%umiext%&#37</span> </div> <div class="col-md col-sm"> <h6>Umidade do Solo</h6> <span class="badge badge-dark">%umisolo% = %umisolostr%</span> </div> <div class="col-md col-sm"> </div> </div> <br> <h2 class="text-center text-muted">Relés</h2> <br> <div class="row text-center"> <div class="col-md col-sm"> </div> <div class="col-md col-sm"> <h6>Umificador</h6> <button type="button" class="badge btn-dark"> Estado: <span class="badge badge-light">%umidificador%</span> </button> </div> <div class="col-md col-sm"> <h6>Bomba Irrigação</h6> <button type="button" class="badge btn-dark"> Estado: <span class="badge badge-light">%bomba%</span> </button> </div> <div class="col-md col-sm"> <h6>Lâmpada</h6> <button type="button" class="badge btn-dark"> Estado: <span class="badge badge-light">%lampada%</span> </button> </div> <div class="col-md col-sm"> <h6>Cooler Frontal</h6> <button type="button" class="badge btn-dark"> Estado: <span class="badge badge-light">%coolerf%</span> </button> </div> <div class="col-md col-sm"> <h6>Cooler Traseiro</h6> <button type="button" class="badge btn-dark"> Estado: <span class="badge badge-light">%coolert%</span> </button> </div> <div class="col-md col-sm"> </div> </div> <br> <h2 class="text-center text-muted">Tempo</h2> <br> <div class="row text-center"> <div class="col-md col-sm"> </div> <div class="col-md col-sm"> <h6>Próximo envio</h6> <button type="button" class="badge btn-dark"><span class="badge badge-light">%tempoenvio% segundo(s)</span> </button> </div> <div class="col-md col-sm"> <h6>Intervalo de irrigação</h6> <button type="button" class="badge btn-dark"><span class="badge badge-light">%tempoirrigacao% segundo(s)</span> </button> </div> <div class="col-md col-sm"> </div> </div> <br> </div></body></html>
)rawliteral";
const String erro404_html = "<!DOCTYPE html><html lang='pt-BR'><head> <title>Estufa IoT</title> <meta charset='UTF-8'> <meta name='viewport' content='width=device-width, initial-scale=1.0'> <meta http-equiv='refresh' content='5'> <link rel='stylesheet' href='https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css' integrity='sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh' crossorigin='anonymous'> <script src='https://code.jquery.com/jquery-3.3.1.slim.min.js' integrity='sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo' crossorigin='anonymous'></script> <script src='https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js' integrity='sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49' crossorigin='anonymous'></script> <script src='https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js' integrity='sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy' crossorigin='anonymous'></script></head><body class='bg-light'> <nav class='navbar navbar-expand-lg navbar-light bg-transparent border-bottom'> <a class='navbar-brand' href='/'><img src='https://i.imgur.com/w0ytmLf.png' class='rounded' alt='Estufa IoT'>Estufa IoT</a> <button class='navbar-toggler' type='button' data-toggle='collapse' data-target='#navbarSupportedContent' aria-controls='navbarSupportedContent' aria-expanded='false' aria-label='Toggle navigation'> <span class='navbar-toggler-icon'></span> </button> <div class='collapse navbar-collapse' id='navbarSupportedContent'> <ul class='navbar-nav mr-auto'> <li class='nav-item'> <a class='nav-link' href='/monitoramento'> <img src='https://i.imgur.com/uNMf99F.png' alt='Monitoramento'> Monitoramento</a> </li> <li class='nav-item'> <a class='nav-link' href='https://thingspeak.com/channels/1031479' target='_blank'> <img src='https://i.imgur.com/oZE1HPC.png' alt=' Canal ThingSpeak'> Canal no ThingSpeak</a> </li> <li class='nav-item'> <a class='nav-link' href='/sobre'> <img src='https://i.imgur.com/6lcZ19z.png' alt='Sobre'> Sobre</a> </li> </ul> </div> </nav> <br> <div class='container'> <h1 class='text-center'><img src=' https://i.imgur.com/qNjvyJr.png' class='img-fluid rounded' alt='Página não encontrada'>Página não encontrada</h1> <br><a class='btn btn-block btn-dark' href='/' role='button'>Retornar ao sistema</a> </form> </div> <br></body></html>";
const String sobre_html = "<!DOCTYPE html><html lang='pt-BR'><head> <title>Estufa IoT</title> <meta charset='UTF-8'> <meta name='viewport' content='width=device-width, initial-scale=1.0'> <link rel='stylesheet' href='https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css' integrity='sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh' crossorigin='anonymous'> <script src='https://code.jquery.com/jquery-3.3.1.slim.min.js' integrity='sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo' crossorigin='anonymous'></script> <script src='https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js' integrity='sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49' crossorigin='anonymous'></script> <script src='https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js' integrity='sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy' crossorigin='anonymous'></script></head><body class='bg-light'> <nav class='navbar navbar-expand-lg navbar-light bg-transparent border-bottom'> <a class='navbar-brand' href='/'><img src='https://i.imgur.com/w0ytmLf.png' class='rounded' alt='Estufa IoT'>Estufa IoT</a> <button class='navbar-toggler' type='button' data-toggle='collapse' data-target='#navbarSupportedContent' aria-controls='navbarSupportedContent' aria-expanded='false' aria-label='Toggle navigation'> <span class='navbar-toggler-icon'></span> </button> <div class='collapse navbar-collapse' id='navbarSupportedContent'> <ul class='navbar-nav mr-auto'> <li class='nav-item'> <a class='nav-link' href='/monitoramento'> <img src='https://i.imgur.com/uNMf99F.png' alt='Monitoramento'> Monitoramento</a> </li> <li class='nav-item'> <a class='nav-link' href='https://thingspeak.com/channels/1031479' target='_blank'> <img src='https://i.imgur.com/oZE1HPC.png' alt=' Canal ThingSpeak'> Canal no ThingSpeak</a> </li> <li class='nav-item'> <a class='nav-link' href='/sobre'> <img src='https://i.imgur.com/6lcZ19z.png' alt='Sobre'> Sobre</a> </li> </ul> </div> </nav> <br> <div class='container '> <h1 class='text-center text-muted'>Sobre</h1><br> <div class='row'> <div class='col-md'> <p>Essa aplicação web consiste em um sistema de controle e monitoramento para uma estufa de hortaliças. As variáveis controladas são: </p> </div> <div class='col-md col-sm'> <ul class='list-group list-group-horizontal'> <li class='list-group-item text-primary'> Umidade relativa do ar</li> <li class='list-group-item text-primary'>Temperatura</li> <li class='list-group-item text-primary'>Umidade do solo</li> <li class='list-group-item text-primary'>Luminosidade</li> </ul><br> </div> </div> <div class='row'> <div class='col-md'> <p> As variáveis são lidas por meio dos sensores DHT22 (umidade e temperatura), um sensor resistivo de umidade do solo e um sensor LDR (Resistor dependente de luz), que detecta a presença ou não de luz. </p> </div> <div class='col-md'> <p>Todos os dados das variáveis são enviados em um intervalo médio de 1 minuto para um canal na plataforma de IoT, ThingSpeak, na qual podem ser monitorados de forma gráfica.</p> </div> </div> <div id='accordion'> <div class='card'> <div class='card-header'> <div class='mb-0'> <button class='btn btn-block' data-toggle='collapse' data-target='#collapseOne' aria-expanded='false' aria-controls='collapseOne'> <img src='https://i.imgur.com/1ohyHJ6.png' alt='Sistema de Luminosidade'> <h6 class=text-center>Sistema de Luminosidade</h6> </button> </div> </div> <div id='collapseOne' class='collapse' aria-labelledby=' ' data-parent='#accordion'> <div class='card-body'> <p>O sistema de iluminação funciona por meio de dois parâmetros, as horas em que deve permanecer ligado, definadas pelo usuário, e pelo sensor LDR. </p> <p> A lâmpada da estufa só permanecerá acessa quando não é detectado nenhuma fonte de luz pelo sensor LDR, instalado na cobertura da estufa e a hora atual deve estar entre a hora inicial e hora final, definadas também pelo usuário. </p> <p class='badge badge-primary text-wrap'> Obs: Os valores para hora inicial e final devem estar entre 1 a 24hrs, considerando 24hrs como a meia-noite.</p> </div> </div> </div> </div> <div id='accordion2'> <div class='card'> <div class='card-header'> <div class='mb-0'> <button class='btn btn-block' data-toggle='collapse' data-target='#collapseTwo' aria-expanded='false' aria-controls='collapseTwo'> <img src='https://i.imgur.com/OCpUqj6.png' alt='Sistema de Luminosidade'> <h6 class=text-center>Sistema de Ventilação</h6> </button> </div> </div> <div id='collapseTwo' class='collapse' aria-labelledby=' ' data-parent='#accordion2'> <div class='card-body'> <p>O Sistema de ventilação funciona com base nos dois sensores de temperatura e umidade relativa do ar junto aos coolers instalados na parte frontal e transeira da estufa.</p> <p>Para diminuir a temperatura da estufa é necessário que as condições externas sejam propícias, deste modo os dois coolers são ligados para troca de calor quando a temperatura interna for maior que a temperatura máxima. </p> <p>Para diminuir a umidade relativa do ar o cooler traseiro é acionado.</p> <p>Para aumentar a umidade relativa do ar é acionado o cooler do umidificador de ar caseiro, localizado dentro da estufa.</p> </div> </div> </div> </div> <div id='accordion3'> <div class='card'> <div class='card-header'> <div class='mb-0'> <button class='btn btn-block' data-toggle='collapse' data-target='#collapseThree' aria-expanded='false' aria-controls='collapseThree'> <img src='https://i.imgur.com/TsiKLcP.png' alt='Sistema de Luminosidade'> <h6 class=text-center>Sistema de Irrigação</h6> </button> </div> </div> <div id='collapseThree' class='collapse' aria-labelledby=' ' data-parent='#accordion3'> <div class='card-body'> <p>O sistema de irrigação funciona com base na leitura do sensor resistivo de umidade do solo, enquanto o sistema detecta que a umidade do solo é igual a 'Seco', inicia-se o processo de irrigação. A umidade é definida de acordo com os valores: </p> <ul class='list-group'> <li class='list-group-item-dark list-group-item'>Maior que 1000 -> Sensor no ar, fora do vaso </li> <li class='list-group-item-secondary list-group-item'>Menor que 1000 e maior ou igual a 800-> Seco</li> <li class='list-group-item-secondary list-group-item'>Menor que 800 e maior ou igual a 500-> Parcialmente úmido</li> <li class='list-group-item-primary list-group-item'> Menor que 500 -> Úmido</li> </ul> <p> A irrigação acontece por gotejamento, uma bomba d'água conduz a água do reservatório para a mangueira, que irriga a planta por pequenos furos, devolvendo o restante da água para o reservatório. Este processo leva cerca de 30 segundos e é repetido num intervalo de 5 minutos, caso seja necessário. </p> </div> </div> </div> </div><br> <p class='font-italic muted text-right'>Ícones de <a href='https://www.flaticon.com/'>Flaticon</a></p> </div></body></html>";

// Portas/Pinos
#define dht_int_pin 0          // Porta digital   D3
#define dht_ext_pin 2          // Porta digital   D4
#define ldr_pin 16             // Porta digital   *D0
#define rele_bomba 14          // Porta digital   D5
#define rele_lampada 12        // Porta digital   D6
#define rele_cooler_traseiro 3 // Porta digital   RX
#define rele_cooler_frontal 13 // Porta digital   D7
#define rele_umidificador 1    // Porta digital   TX
#define umi_solo_pin A0        // Porta analôgica A0

// Configurações de relógio e sensores
WiFiUDP udp;                                      // Cria um objeto da classe UDP
NTPClient ntp(udp, "a.ntp.br", -3 * 3600, 60000); // Cria um objeto "NTP" com as configurações do padrão brasileiro | a.ntp.br Servidor NTP Funcionando 05/05/2020
DHT dht_int(dht_int_pin, DHT22);                  // Sensor DHT Interno
DHT dht_ext(dht_ext_pin, DHT22);                  // Sensor DHT Externo
WiFiClient client;

// Variáveis
int umi_solo;   // Umidade do Solo
int hora_atual; // Dois primeiros digitos apenas
int irrigando = 0;
float umi_ar_int; // Umidade Relativa do Ar Interna
float temp_int;   // Temperatura Interna
float umi_ar_ext; // Umidade Relativa do Ar Externa
float temp_ext;   // Temperatura Externa
String horario;   // Hora formatada com minutos e segundos
String umi_solo_str;
String parametros;
boolean umidificando = false;
boolean ventilando = false;
unsigned long tempo_envio = 0;
unsigned long tempo_irrigacao = 0;
const unsigned long intervalo = 60000L;            // 1 minuto
const unsigned long intervalo_irrigacao = 300000L; // 5 minutos

// Inputs da página WEB
int inputs[5];
int temp_min;
int temp_max;
int umi_min;
int umi_max;
int hora_inicial;
int hora_final;

// Funções
void getDados();
void conectarWifi();
void verificarConexao();
void luminosidade();
void ventilacao();
void irrigacao();
void umidade();
void carregarParametros();
void erro404(AsyncWebServerRequest *request);
void salvarArquivo(fs::FS &fs, const char *path, const char *message);
int enviarDados(long TSChannel, float data1, float data2, float data3, float data4, int data5);
String lerArquivo(fs::FS &fs, const char *path);
String parametrosWeb(const String &var);
String parametrosMonitoramento(const String &var);

// Configurações de Rede (SSID e Senha)
const char *ssid = "ESP8266";
const char *senha = "loguinha";
IPAddress ip(192, 168, 1, 175);
IPAddress gateway(192, 168, 1, 1);
IPAddress subnet(255, 255, 255, 0);
IPAddress dns(8, 8, 4, 4);

// Configurações do ThingSpeak
unsigned long idCanal = 1031479;         // ID do Canal do ThingSpeak
char *chaveEscrita = "9HCNY93RZQ12O0BI"; // Chave de Escrita

void setup()
{

    // Inicializa o SPIFFS (SPI Flash File System)
    if (!SPIFFS.begin())
    {
        // Erro ao montar o SPIFFS
        return;
    }

    conectarWifi();
    WiFi.mode(WIFI_STA); // Define o ESP8266 como Station.
    dht_int.begin();
    dht_ext.begin();

    // Modo das portas, OUTPUT/INPUT
    pinMode(umi_solo_pin, INPUT);
    pinMode(ldr_pin, INPUT);
    pinMode(rele_lampada, OUTPUT);
    pinMode(rele_cooler_traseiro, OUTPUT);
    pinMode(rele_cooler_frontal, OUTPUT);
    pinMode(rele_umidificador, OUTPUT);
    pinMode(rele_bomba, OUTPUT);

    carregarParametros(); // Carrega na inicialização

    ntp.begin();       // Inicia o protocolo
    ntp.forceUpdate(); // Força atualização

    // Página Monitoramento
    server.on("/monitoramento", HTTP_GET, [](AsyncWebServerRequest *request) {
        request->send_P(200, "text/html", monitoramento_html, parametrosMonitoramento);
    });

    // Página inicial
    server.on("/", HTTP_GET, [](AsyncWebServerRequest *request) {
        request->send_P(200, "text/html", index_html, parametrosWeb);
    });

    // Página sobre
    server.on("/sobre", HTTP_GET, [](AsyncWebServerRequest *request) {
        request->send(200, "text/html", sobre_html);
    });

    // Solicitação GET (Submit do Form)
    server.on("/get", HTTP_GET, [](AsyncWebServerRequest *request) {
        if ((request->hasParam("input1")) && (request->hasParam("input2")) && (request->hasParam("input3")) && (request->hasParam("input4")) && (request->hasParam("input5")) && (request->hasParam("input6")))
        {
            // Recebe os inputs convertendo string para inteiro
            temp_min = atoi(request->getParam("input1")->value().c_str());
            temp_max = atoi(request->getParam("input2")->value().c_str());
            umi_min = atoi(request->getParam("input3")->value().c_str());
            umi_max = atoi(request->getParam("input4")->value().c_str());
            hora_inicial = atoi(request->getParam("input5")->value().c_str());
            hora_final = atoi(request->getParam("input6")->value().c_str());

            String dadosRequest = request->getParam("input1")->value() + "/" + request->getParam("input2")->value() + "/" + request->getParam("input3")->value() + "/" + request->getParam("input4")->value() + "/" + request->getParam("input5")->value() + "/" + request->getParam("input6")->value() + "/";

            salvarArquivo(SPIFFS, "/dados.txt", dadosRequest.c_str()); // Grava os parametros num arquivo txt dentro da memória flash.
        }

        carregarParametros(); // Carrega após submit (get)
    });

    server.onNotFound(erro404); // Página não encontrada
    server.begin();
}

void loop()
{
    verificarConexao();
    getDados();

    // Verificar atuadores
    umidade();
    luminosidade();
    ventilacao();
    irrigacao();

    // Enviar os dados para o ThingSpeak
    if (millis() - tempo_envio >= intervalo)
    {
        tempo_envio = millis();
        enviarDados(idCanal, temp_int, temp_ext, umi_ar_int, umi_ar_ext, umi_solo);
    }
}

void ventilacao()
{

    // Diminuir a temperatura
    if (umidificando == false)
    {
        if (temp_int > temp_max)
        {
            digitalWrite(rele_cooler_frontal, HIGH);
            digitalWrite(rele_cooler_traseiro, HIGH);
            ventilando = true;
        }
        else
        {
            digitalWrite(rele_cooler_frontal, LOW);
            ventilando = false;
        }
    }
    else
    {
        digitalWrite(rele_cooler_frontal, LOW);
        digitalWrite(rele_cooler_traseiro, LOW);
        ventilando = false;
    }
}

void umidade()
{

    // Aumentar Umidade
    if (umi_ar_int < umi_min)
    {
        digitalWrite(rele_umidificador, HIGH);
        umidificando = true;
    }
    else
    {
        digitalWrite(rele_umidificador, LOW);
        umidificando = false;
    }

    // Diminuir Umidade
    if (umidificando == false)
    {
        if (umi_ar_int > umi_max)
        {
            digitalWrite(rele_cooler_traseiro, HIGH);
        }
        else
        {
            digitalWrite(rele_cooler_traseiro, LOW);
        }
    }
    else
    {
        digitalWrite(rele_cooler_traseiro, LOW);
    }
}
void irrigacao()
{

    if (millis() - tempo_irrigacao >= intervalo_irrigacao)
    {
        tempo_irrigacao = millis();
        if (umi_solo_str == "Seco")
        {
            irrigando = 1;
            digitalWrite(rele_bomba, HIGH);
            delay(30000);
            digitalWrite(rele_bomba, LOW);
        }
    }
    else
    {
        irrigando = 0;
    }
}

void luminosidade()
{

    if (hora_inicial == 24)
    {
        hora_inicial = 0;
    }
    if (hora_atual >= hora_inicial && hora_atual < hora_final)
    {
        if (digitalRead(ldr_pin) == HIGH) // Não tem luz
        {
            digitalWrite(rele_lampada, HIGH);
        }
        else
        {
            digitalWrite(rele_lampada, LOW);
        }
    }
    else
    {
        digitalWrite(rele_lampada, LOW);
    }
}

void getDados()
{
    delay(2000); // Tempo necessário para leitura dos sensores
    temp_int = dht_int.readTemperature();
    umi_ar_int = dht_int.readHumidity();
    temp_ext = dht_ext.readTemperature();
    umi_ar_ext = dht_ext.readHumidity();
    umi_solo = analogRead(umi_solo_pin);
    horario = ntp.getFormattedTime();
    hora_atual = ntp.getHours();

    if (umi_solo >= 1000)
    {
        umi_solo_str = "Sensor no ar, fora do vaso";
    }
    else if (umi_solo < 1000 && umi_solo >= 800)
    {
        umi_solo_str = "Seco";
    }
    else if (umi_solo < 800 && umi_solo >= 500)
    {
        umi_solo_str = "Parcialmente umido";
    }
    else if (umi_solo < 500)
    {
        umi_solo_str = "Umido";
    }
}

void conectarWifi()
{

    WiFi.begin(ssid, senha);
    WiFi.config(ip, gateway, subnet, dns);

    while (WiFi.status() != WL_CONNECTED)
    {
        delay(2000);
        // Aguardando rede...
    }

    ThingSpeak.begin(client);
}

// Envia vários campos simultaneamente ao ThingSpeak
int enviarDados(long TSChannel, float data1, float data2, float data3, float data4, int data5)
{
    ThingSpeak.setField(1, data1); // Temperatura Interna
    ThingSpeak.setField(2, data2); // Temperatura Externa
    ThingSpeak.setField(3, data3); // Umidade Relativa do Ar Interna
    ThingSpeak.setField(4, data4); // Umidade Relativa do Ar Externa
    ThingSpeak.setField(5, data5); // Umidade do Solo

    int writeSuccess = ThingSpeak.writeFields(TSChannel, chaveEscrita);

    return writeSuccess;
}

void verificarConexao()
{
    //  Verifica se caiu a conexão e força a conexão novamente.
    if (WiFi.status() != WL_CONNECTED)
    {
        // Conexão Perdida, reconectando
        conectarWifi();
    }
}

void erro404(AsyncWebServerRequest *request)
{
    request->send(404, "text/html", erro404_html);
}

String lerArquivo(fs::FS &fs, const char *path)
{
    // Lendo o arquivo: %s\r\n", path)
    File file = fs.open(path, "r");
    if (!file || file.isDirectory())
    {
        // Arquivo vazio, ou falha ao abrir o arquivo.
        return String();
    }
    // Lendo o arquivo
    String fileContent;
    while (file.available())
    {
        fileContent += String((char)file.read());
    }
    parametros = fileContent;
    return fileContent;
}

void salvarArquivo(fs::FS &fs, const char *path, const char *message)
{
    // Escrevendo arquivo: %s\r\n", path
    File file = fs.open(path, "w");
    if (!file)
    {
        // Falha ao abrir o arquivo para gravação
        return;
    }
    if (file.print(message))
    {
        // Arquivo escrito!
    }
    else
    {
        // Gravação falhou!
    }
}

// Seta os valores dos parâmetros na página WEB (Monitoramento)
String parametrosMonitoramento(const String &var)
{
    char buffer[30];
    if (var == "tempint")
    {
        return String(temp_int);
    }
    if (var == "tempext")
    {
        return String(temp_ext);
    }
    if (var == "umiint")
    {
        return String(umi_ar_int);
    }
    if (var == "umiext")
    {
        return String(umi_ar_ext);
    }
    if (var == "coolerf")
    {
        return itoa(digitalRead(rele_cooler_frontal), buffer, 10);
    }
    if (var == "coolert")
    {
        return itoa(digitalRead(rele_cooler_traseiro), buffer, 10);
    }
    if (var == "umidificador")
    {
        return itoa(digitalRead(rele_umidificador), buffer, 10);
    }
    if (var == "bomba")
    {
        return itoa(irrigando, buffer, 10);
    }
    if (var == "lampada")
    {
        return itoa(digitalRead(rele_lampada), buffer, 10);
    }
    if (var == "horario")
    {
        return horario;
    }
    if (var == "umisolo")
    {
        return itoa(umi_solo, buffer, 10);
    }
    if (var == "umisolostr")
    {
        return umi_solo_str;
    }
    if (var == "tempoirrigacao")
    {
        return String(((intervalo_irrigacao - millis() + tempo_irrigacao) / 1000));
    }
    if (var == "tempoenvio")
    {
        return String(((intervalo - millis() + tempo_envio) / 1000));
    }
}

// Seta os valores dos parâmetros na página WEB
String parametrosWeb(const String &var)
{
    char buffer[30];
    if (var == "tempmin")
    {
        return itoa(temp_min, buffer, 10);
    }
    if (var == "tempmax")
    {
        return itoa(temp_max, buffer, 10);
    }
    if (var == "umimin")
    {
        return itoa(umi_min, buffer, 10);
    }
    if (var == "umimax")
    {
        return itoa(umi_max, buffer, 10);
    }
    if (var == "horainicial")
    {
        return itoa(hora_inicial, buffer, 10);
    }
    if (var == "horafinal")
    {
        return itoa(hora_final, buffer, 10);
    }
}

void carregarParametros()
{

    String param_aux = "";
    int j = 0;

    parametros = lerArquivo(SPIFFS, "/dados.txt");

    for (int i = 0; i < parametros.length(); i++)
    {
        if (parametros.charAt(i) == '/')
        {
            inputs[j] = atoi(param_aux.c_str());
            param_aux = "";
            i++;
            j++;
        }
        param_aux = param_aux + parametros.charAt(i);
    }

    temp_min = inputs[0];
    temp_max = inputs[1];
    umi_min = inputs[2];
    umi_max = inputs[3];
    hora_inicial = inputs[4];
    hora_final = inputs[5];
}
